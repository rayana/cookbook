# Spaguetti Bolognese
I always thought I knew how to make bolognese: minced beef, onions, tomatoes, and some herbs. Right? What a surprise to see the real list of ingredientes once I, for a miraculous reason, decided to look up for the recipe in one of my cookbooks: [Panelina - Receitas Que Funcionam](https://www.amazon.com.br/Panelinha-Receitas-funcionam-Rita-Lobo/dp/8539602776) by Rita Lobo. Obviously I had to adapt the recipe to my own standards and use the ingredients I had in hand. The end product resulted in a "italian like" dish. I still don't feel like I am serving a true italian spaghetti-bolognese, and I probably never will, but this recipe was a success in our house. That's good enough to me.

* Serves: 4-5
* Preparation time: 5 min
* Cooking time: 60 min

## Ingredients
- 1 tbsp olive oil
- 2 onions, chopped
- 3 garlic cloves, crushed
- Mix of dried spices (I used oregano, parsley, bay leaf, and basil)
- 1kg of beef mince
- 400 ml of milk
- 2 x 400g cans peeled tomatoes
- 200 ml white wine
- 500 ml vegetable broth
- Salt and pepper to taste
- Dried spaguetti
- Freshly grated parmesan cheese, to serve

## Directions
- Heat the oil in a large, heavy-based saucepan and add the onions and garlic, frying until softened.
- Increase the heat and add the minced beef. Add the mix of spices, salt, and pepper, and fry it until it has browned.
- Stir in the milk and cook until most of it evaporates.
- Add the white wine and cook until it evaporates.
- Reduce the heat and pour in the peeled tomatoes and the vegetable broth. Cook for 30 minutes or until it’s rich and thickened, stirring occasionally.
- Cook the pasta in plenty of boiling salted water. Drain and divide between plates, adding a good ladleful of the sauce. Finish with parmesan cheese.